import React from 'react';
import './styles/Home.css';
import Image from 'react-bootstrap/Image';
import Carousel from 'react-bootstrap/Carousel';
import Product from './Product';
import data from '../data/productos.json';
import Footer from './Footer';

const Home = () => {
  const products = data.slice(0, 20); // Obtén los primeros 20 productos

  const chunkSize = 3; // Cantidad de productos por página
  const pages = Math.ceil(products.length / chunkSize);

  // Divide los productos en páginas
  const productPages = Array.from({ length: pages }, (_, index) =>
    products.slice(index * chunkSize, (index + 1) * chunkSize)
  );

  return (
    <div className="cont">
      <div className='baner'>
        <Image src="img/ver_todo_desk.jpg" />
      </div>
      <div className='container'>
        <h1>¡Bienvenido a nuestro catálogo de productos!</h1>
        <p>¡Descubre nuestro catálogo en línea de herramientas! Te invitamos a explorar nuestro sitio web y sumergirte en un mundo lleno de soluciones prácticas y eficientes para tus necesidades. Desde herramientas básicas hasta dispositivos especializados, tenemos todo lo que necesitas para hacer tus tareas más fáciles y efectivas.</p>
        <p><strong>REVISA NUESTRAS OFERTAS!</strong></p>
      </div>
      <Carousel>
        {productPages.map((page, pageIndex) => (
          <Carousel.Item key={pageIndex}>
            <div className="d-flex justify-content-center">
              {page.map((product) => (
                <Product key={product.id} product={product} />
              ))}
            </div>
          </Carousel.Item>
        ))}
      </Carousel>
      <hr />
      <Footer />
    </div>
  );
};

export default Home;
