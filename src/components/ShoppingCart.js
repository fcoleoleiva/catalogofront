import React, { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import './styles/ShopingCart.css';
import Card from 'react-bootstrap/Card';
import { useNavigate } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';

const ShoppingCart = () => {
  const [cartItems, setCartItems] = useState([]);
  const [total, setTotal] = useState(0);
  const [showModal, setShowModal] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    var keys = Object.keys(localStorage);
    var storedCartItems = [];

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var value = localStorage.getItem(key);
      storedCartItems.push({ id: key, product: value });
    }

    if (storedCartItems.length > 0) {
      setCartItems(storedCartItems);

      // Calcular el total de los precios al cargar los elementos del carrito
      const prices = storedCartItems.map((item) => {
        const product = JSON.parse(item.product);
        return product.precio * item.quantity;
      });
      const totalPrice = prices.reduce((accumulator, currentPrice) => accumulator + currentPrice, 0);
      setTotal(totalPrice);
    }
  }, []);

  const openModal = () => {
    setShowModal(true);
  };

  const removeFromCart = (id) => {
    // Eliminar el elemento del localStorage utilizando solo la clave (id)
    localStorage.removeItem(id);

    // Actualizar el estado de cartItems excluyendo el elemento eliminado
    const updatedCartItems = cartItems.filter((item) => item.id !== id);
    setCartItems(updatedCartItems);

    // Calcular el nuevo total después de eliminar un elemento del carrito
    const prices = updatedCartItems.map((item) => {
      const product = JSON.parse(item.product);
      return product.precio * item.quantity;
    });
    const totalPrice = prices.reduce((accumulator, currentPrice) => accumulator + currentPrice, 0);
    setTotal(totalPrice);

    openModal();
  };

  const handleQuantityChange = (event, id) => {
    const newQuantity = parseInt(event.target.value);
    if (newQuantity > 0) {
      const updatedCartItems = cartItems.map((item) => {
        if (item.id === id) {
          return { ...item, quantity: newQuantity };
        }
        return item;
      });
      setCartItems(updatedCartItems);

      // Calcular el nuevo total después de cambiar la cantidad de un elemento en el carrito
      const prices = updatedCartItems.map((item) => {
        const product = JSON.parse(item.product);
        return product.precio * item.quantity;
      });
      const totalPrice = prices.reduce((accumulator, currentPrice) => accumulator + currentPrice, 0);
      setTotal(totalPrice);
    }
  };

  const handleCheckout = () => {
    if (!isNaN(total) && total > 0) {

      // Redirigir a otra página
      navigate('/pedido');
    }
  };

  const handleGetMoreProducts = () => {
    // Redirigir a la página de productos
    navigate('/productos');
  };

  return (
    <div className='principio'>
      <h2>CARRITO DE COMPRAS</h2>
      {cartItems.length > 0 && <h3>Ingrese las cantidades deseadas</h3>}
      <div className='cuerpo'>
        <div className='mama'>
          <Card>
            <Card.Body>
              {cartItems.length === 0 ? (
                <p>No hay productos en el carrito.</p>
              ) : (
                <>
                  <ul className='noOrdenada'>
                    {cartItems.map((item) => {
                      const product = JSON.parse(item.product);
                      return (
                        <li className='lista' key={item.id}>
                          <div className='botonCarrito'>
                            <Button variant='danger' onClick={() => removeFromCart(item.id)}>
                              Eliminar
                            </Button>
                          </div>

                          <div className='inputCarrito'>
                            <label htmlFor={`quantity-${item.id}`}>Cantidad:</label>
                            <input
                              type='number'
                              id={`quantity-${item.id}`}
                              min={1}
                              value={item.quantity}
                              onChange={(event) => handleQuantityChange(event, item.id)}
                            />
                          </div>

                          <span>{product.nombre} - Precio: $ {product.precio}</span>
                        </li>
                      );
                    })}
                  </ul>
                  <p>Total: $ {isNaN(total) ? 0 : total}</p>
                  <div className='checkout-button'>
                    <Button variant='primary' onClick={handleCheckout} disabled={isNaN(total) || total <= 0}>
                      Pagar
                    </Button>
                    <Button variant='secondary' onClick={handleGetMoreProducts}>
                      Conseguir más productos
                    </Button>
                  </div>
                </>
              )}
            </Card.Body>
          </Card>
        </div>
      </div>
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>OK!</Modal.Title>
        </Modal.Header>
        <Modal.Body>Producto eliminado del carrito</Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setShowModal(false)}>Cerrar</Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ShoppingCart;
