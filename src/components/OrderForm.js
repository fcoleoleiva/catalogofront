import React, { useState } from 'react';
import './styles/OrderForm.css';
import Card from 'react-bootstrap/Card';
import { Button, Modal, InputGroup, FormControl } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Image from 'react-bootstrap/Image';

const OrderForm = () => {
  const [formData, setFormData] = useState({
    nombreApellido: '',
    direccion: '',
    numeroCasa: '',
    contacto: '',
  });

  const [showConfirmationModal, setShowConfirmationModal] = useState(false);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [formComplete, setFormComplete] = useState(false);

  const handleInputChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });

    // Verificar si todos los campos están completos
    const { nombreApellido, direccion, numeroCasa, contacto } = formData;
    if (nombreApellido !== '' && direccion !== '' && numeroCasa !== '' && contacto !== '') {
      setFormComplete(true);
    } else {
      setFormComplete(false);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();


    // Mostrar el modal de confirmación
    setShowConfirmationModal(true);
  };

  const handleConfirmationModalClose = (accepted) => {
    if (accepted) {
     
      setShowSuccessModal(true);
      localStorage.clear();
      setShowConfirmationModal(false);
      // Reiniciar el carrito de compras
      localStorage.removeItem('carrito');
    } else {
      // Cerrar el modal de confirmación y permitir modificar los datos
      setShowConfirmationModal(false);
    }
  };

  const handleSuccessModalClose = () => {
    // Cerrar el modal de éxito
    setShowSuccessModal(false);
  };

  return (
    <div className='container'>
    <h1>Dirección de envío</h1>
    <div className='dir mt-5'>

      <div className='formulario-direccion'>
        <Card>
          <Card.Body>
            <form onSubmit={handleSubmit}>
              <div className='nombre-apellido'>
                <InputGroup className='mb-3'>
                  <InputGroup.Text>Nombre y Apellido:</InputGroup.Text>
                  <FormControl
                    type='text'
                    id='nombreApellido'
                    name='nombreApellido'
                    placeholder='Nombre Apellido'
                    value={formData.nombreApellido}
                    onChange={handleInputChange}
                    required
                  />
                </InputGroup>
              </div>
              <div className='direccion'>
                <InputGroup className='mb-3'>
                  <InputGroup.Text>Dirección de envío:</InputGroup.Text>
                  <FormControl
                    type='text'
                    id='direccion'
                    placeholder='Calle bonita'
                    name='direccion'
                    value={formData.direccion}
                    onChange={handleInputChange}
                    required
                  />
                </InputGroup>
              </div>
              <div className='numero-casa'>
                <InputGroup className='mb-3'>
                  <InputGroup.Text>Número de casa:</InputGroup.Text>
                  <FormControl
                    type='text'
                    id='numeroCasa'
                    name='numeroCasa'
                    placeholder='141-B'
                    value={formData.numeroCasa}
                    onChange={handleInputChange}
                    required
                  />
                </InputGroup>
              </div>
              <div className='contacto'>
                <InputGroup className='mb-3'>
                  <InputGroup.Text>Número de contacto:</InputGroup.Text>
                  <FormControl
                    type='text'
                    id='contacto'
                    placeholder='+569-XXXXXXX'
                    name='contacto'
                    value={formData.contacto}
                    onChange={handleInputChange}
                    required
                  />
                </InputGroup>
              </div>
              <div className='botonPedido'>
                <Button type='submit' disabled={!formComplete}>
                  Realizar pedido
                </Button>
              </div>
            </form>
          </Card.Body>
        </Card>
      </div>

            
      <div>
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1619.791976372247!2d-71.59003948360687!3d-33.03340423449832!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9689e0ca402b315b%3A0x61fa04dd766cdccb!2sINACAP%20Sede%20Valpara%C3%ADso!5e0!3m2!1ses!2scl!4v1688522712141!5m2!1ses!2scl"
        width="600"
        height="450"
        style={{ border: 0 }}
        allowFullScreen=""
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      ></iframe>
      </div>

      {/* Modal de confirmación */}
      <Modal show={showConfirmationModal} onHide={() => handleConfirmationModalClose(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Confirmar pedido</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Nombre y Apellido: {formData.nombreApellido}</p>
          <p>Dirección de envío: {formData.direccion}</p>
          <p>Número de casa: {formData.numeroCasa}</p>
          <p>Detalles de contacto: {formData.contacto}</p>
          <p>¿Está seguro de que desea enviar el pedido con esta información?</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={() => handleConfirmationModalClose(false)}>
            No
          </Button>
          <Button variant='primary' onClick={() => handleConfirmationModalClose(true)}>
            Sí
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Modal de éxito */}
      <Modal show={showSuccessModal} onHide={handleSuccessModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Pedido ingresado con éxito</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Tu pedido ha sido ingresado con éxito. ¡Gracias por tu compra!</p>
        </Modal.Body>
        <Modal.Footer>
          <Link to="/">
            <Button variant='primary' onClick={handleSuccessModalClose}>
              Cerrar
            </Button>
          </Link>
        </Modal.Footer>
      </Modal>
    </div>
    </div>
  );
};

export default OrderForm;
