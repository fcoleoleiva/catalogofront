import React from 'react';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';
import './styles/Navigation.css'
import Nav from 'react-bootstrap/Nav';
import { BsHouse, BsGrid, BsCart } from 'react-icons/bs';

const Navigation = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div className='aq'>
      <div className='containe'>

      <Nav variant="tabs" defaultActiveKey="/home">
      <Nav.Item>
        <Nav.Link href="/" className="d-flex align-items-center">
          <BsHouse className="nav-icon me-2" />
          Home
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/productos" className="d-flex align-items-center">
          <BsGrid className="nav-icon me-2" />
          Productos
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/carrito" className="d-flex align-items-center">
          <BsCart className="nav-icon me-2" />
          Carrito
        </Nav.Link>
      </Nav.Item>
    </Nav>
      </div>
    </div>
  );
};

export default Navigation;
