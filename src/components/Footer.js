import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import './styles/Footer.css';

const Footer = () => {
  return (
    <div className='container'>

    <footer className="footer">
                <Card>
          <Card.Body>

          </Card.Body>
      <Container>
        <Row>
          <Col md={6}>
            <h3>Contacto</h3>
            <Form>
              <Form.Group controlId="formName">
                <Form.Label>Nombre</Form.Label>
                <Form.Control type="text" placeholder="Ingrese su nombre" />
              </Form.Group>
              <Form.Group controlId="formName">
                <Form.Label>Correo electronico</Form.Label>
                <Form.Control type="text" placeholder="Ingrese su correo" />
              </Form.Group>
              <Form.Group className='mb-4' controlId="formMessage">
                <Form.Label>Mensaje</Form.Label>
                <Form.Control as="textarea" rows={3} placeholder="Ingrese su mensaje" />
              </Form.Group>
              <Button variant="primary" type="submit">
                Enviar
              </Button>
            </Form>
          </Col>
          <Col md={6}>
            <h3>Herramientas especiales</h3>
            <p>
              Si desea realizar una cotización de herramientas especiales, por favor, complete el formulario de contacto y
              bríndenos los detalles de sus requerimientos. Nos pondremos en contacto con usted a la brevedad posible.
            </p>
          </Col>
        </Row>
      </Container>
        </Card>
    </footer>
    </div>
  );
};

export default Footer;
