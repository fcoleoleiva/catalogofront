import React, { useState, useEffect } from 'react';
import Product from './Product';
import data from '../data/productos.json';
import './styles/ProductCatalog.css';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Pagination from 'react-bootstrap/Pagination';
import Image from 'react-bootstrap/Image';
import Footer from './Footer';

const ProductCatalog = () => {
  const [products, setProducts] = useState([]);
  const [category, setCategory] = useState('');
  const [searchTerm, setSearchTerm] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [productsPerPage] = useState(10);

  useEffect(() => {
    setProducts(data);
  }, []);

  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
    setCurrentPage(1);
  };

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    setCurrentPage(1);
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const filteredProducts = products.filter((product) => {
    return (
      (category === '' || product.categoria === category) &&
      (searchTerm === '' ||
        product.nombre.toLowerCase().includes(searchTerm.toLowerCase()) ||
        product.descripcion.toLowerCase().includes(searchTerm.toLowerCase()))
    );
  });

  const totalPages = Math.ceil(filteredProducts.length / productsPerPage);
  const pageNumbers = [];
  for (let i = 1; i <= totalPages; i++) {
    pageNumbers.push(i);
  }

  const indexOfLastProduct = currentPage * productsPerPage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
  const currentProducts = filteredProducts.slice(
    indexOfFirstProduct,
    indexOfLastProduct
  );

  return (
    
    <div>
      <div className='baner'>
      <Image src="img/ver_todo_desk.jpg" />
      </div>
      <div className='filters'>
    <div className='category'>
    <Form.Select  id='category' value={category} onChange={handleCategoryChange}>
    <option value=''>Todas las categorias</option>
    <option value='Herramientas manuales'>Herramientas manuales</option>
    <option value='Herramientas de medición'>Herramientas de medición</option>
    <option value='Herramientas de acceso'>Herramientas de acceso</option>
    <option value='Herramientas de albañilería'>Herramientas de albañilería</option>
    <option value='Herramientas de jardinería'>Herramientas de jardinería</option>
    <option value='Herramientas eléctricas'>Herramientas eléctricas</option>
    <option value='Herramientas de detección'>Herramientas de detección</option>
    <option value='Herramientas de sujeción'>Herramientas de sujeción</option>
    <option value='Accesorios y almacenamiento'>Accesorios y almacenamiento</option>
    <option value='Accesorios y consumibles'>Accesorios y consumibles</option>
    <option value='Herramientas de jardinería'>Herramientas de jardinería</option>
    <option value='Herramientas de medición'>Herramientas de medición</option>
    <option value='Herramientas de corte'>Herramientas de corte</option>
    <option value='Herramientas de extracción'>Herramientas de extracción</option>
    </Form.Select>
    </div>

        <div className='buscar'>
          <InputGroup className='mb-3' id='search'>
            <InputGroup.Text>Buscar</InputGroup.Text>
            <Form.Control
              aria-label='Default'
              aria-describedby='inputGroup-sizing-default'
              value={searchTerm}
              onChange={handleSearchChange}
            />
          </InputGroup>
        </div>
      </div>

      <div className='container'>
        {currentProducts.map((product) => (
          <Product key={product.nombre} product={product} />
        ))}
      </div>

      <div className='pagination container'>
        <Pagination>
          {pageNumbers.map((pageNumber) => (
            <Pagination.Item
              key={pageNumber}
              active={pageNumber === currentPage}
              onClick={() => handlePageChange(pageNumber)}
            >
              {pageNumber}
            </Pagination.Item>
          ))}
        </Pagination>

      </div>
      <div>
        <hr />
      <Footer />
        </div>
    </div>
  );
};

export default ProductCatalog;
