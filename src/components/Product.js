import './styles/Product.css'
import React, { useState } from 'react';
import { Alert, Button } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import { useNavigate } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';


const Product = ({ product }) => {
  const { id, nombre, descripcion, precio } = product;
  const [showModal, setShowModal] = useState(false);
  
  const navigate = useNavigate();

  const openModal = () => {
    setShowModal(true);
  };


  const handleAddToCart = () => {
    // Obtén el objeto del producto
    const { id, nombre, descripcion, precio } = product;
  
    // Crea un nuevo objeto sin la propiedad "id"
    const productoSinId = { nombre, descripcion, precio };
  
    // Guarda el producto en el localStorage
    localStorage.setItem(id.toString(), JSON.stringify(productoSinId));
    openModal();

  };

  const handleGoToCart = () => {
    // Redirigir a la página del carrito
    navigate('/carrito');
  };
  
 

  return (
    <div className='conta'>   
    <div className='container'>
      <Card style={{ width: '18rem' }}>
        <Card.Body>
          <div className='cardTitle'>
          <Card.Title><strong>{nombre}</strong></Card.Title>
          </div>
          <Card.Text>
          {descripcion}
          <p>Precio: ${precio}</p>
          </Card.Text>
          <div className='botonCard'>
          <Button  onClick={handleAddToCart}>Agregar al carrito</Button>
          </div>
        </Card.Body>
        </Card>
    </div> 
    <Modal show={showModal} onHide={() => setShowModal(false)}>
      <Modal.Header closeButton>
        <Modal.Title>OK!</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Producto agregado al carrito
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleGoToCart}>
          Ir al carrito
        </Button>
        <Button onClick={() => setShowModal(false)}>Agregar mas productos</Button>
      </Modal.Footer>
    </Modal>  
    </div>

  );
};

export default Product;
