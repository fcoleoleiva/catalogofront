import React from 'react';
import Navigation from './components/Navigation';
import ProductCatalog from './components/ProductCatalog';
import ShoppingCart from './components/ShoppingCart';
import Footer from './components/Footer.js';
import OrderForm from './components/OrderForm';
import Home from './components/Home';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';




const App = () => {
  return (
    <Router>
      <div style={{ backgroundColor: '#f2f2f2' }}>
        <Navigation /> {}
        
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/productos" element={<ProductCatalog />} />
          <Route path="/carrito" element={<ShoppingCart />} />
          <Route path="/pedido" element={<OrderForm />} />
        </Routes>

      </div>
      <div>
      </div>
    </Router>
  );
};



export default App;
